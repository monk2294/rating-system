namespace UniversityRatingSystem.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Added_ColIndex_to_Marks : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Marks", "ColIndex", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Marks", "ColIndex");
        }
    }
}
