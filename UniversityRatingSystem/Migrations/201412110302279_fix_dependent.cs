namespace UniversityRatingSystem.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class fix_dependent : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Documents", "Creator_Id", "dbo.AspNetUsers");
            DropIndex("dbo.Documents", new[] { "Creator_Id" });
            AlterColumn("dbo.Documents", "Creator_Id", c => c.String(maxLength: 128));
            CreateIndex("dbo.Documents", "Creator_Id");
            AddForeignKey("dbo.Documents", "Creator_Id", "dbo.AspNetUsers", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Documents", "Creator_Id", "dbo.AspNetUsers");
            DropIndex("dbo.Documents", new[] { "Creator_Id" });
            AlterColumn("dbo.Documents", "Creator_Id", c => c.String(nullable: false, maxLength: 128));
            CreateIndex("dbo.Documents", "Creator_Id");
            AddForeignKey("dbo.Documents", "Creator_Id", "dbo.AspNetUsers", "Id");
        }
    }
}
